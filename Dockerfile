# vim:set ft=dockerfile:
FROM debian:jessie

COPY jdk-7u79-linux-x64.gz /tmp/jdk-7u79-linux-x64.gz

RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get install curl -y \
    && apt-get clean


RUN echo debconf shared/accepted-oracle-license-v1-1 select true | debconf-set-selections \
    && echo debconf shared/accepted-oracle-license-v1-1 seen true | debconf-set-selections \
    && tar -xvf /tmp/jdk-7u79-linux-x64.gz \
    && mkdir -p /usr/lib/jvm \
    && mv ./jdk1.7.0_79/ /usr/lib/jvm/jdk \
    && rm -rf /tmp/jdk* \
    && update-alternatives --install "/usr/bin/java" "java" "/usr/lib/jvm/jdk/bin/java" 1 \
    && update-alternatives --install "/usr/bin/javac" "javac" "/usr/lib/jvm/jdk/bin/javac" 1 \
    && update-alternatives --install "/usr/bin/javaws" "javaws" "/usr/lib/jvm/jdk/bin/javaws" 1 \
    && chmod a+x /usr/bin/java \
    && chmod a+x /usr/bin/javac \
    && chmod a+x /usr/bin/javaws \
    && update-alternatives --config java \
    && update-alternatives --config javac \
    && update-alternatives --config javaws \
    && echo "JAVA_HOME=\"/usr/lib/jvm/jdk\"" >> /etc/environment \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*




